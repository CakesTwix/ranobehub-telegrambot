from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, UniqueConstraint, BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy.orm import declarative_base

Base = declarative_base()

class Subscribers(Base):
    __tablename__ = "subscribers"
    id = Column(Integer, primary_key=True)
    book_id = Column(Integer)
    book_name = Column(String)
    last_chapter = Column(Integer)
    user_id = Column(Integer, ForeignKey('user.user_id'))
    user = relationship("User", backref='user')
    __table_args__ = UniqueConstraint('book_id', 'user_id', name='book_id_check'),


class User(Base):
    __tablename__ = "user"
    user_id = Column(BigInteger, primary_key=True, unique=True, autoincrement=False)