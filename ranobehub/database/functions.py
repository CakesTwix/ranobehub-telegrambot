from sqlalchemy.ext.asyncio import create_async_engine
from .models import User, Base
from sqlalchemy import insert

class DB():

    def __init__(self):
        self.engine = create_async_engine(
            "sqlite+aiosqlite:///db.sqlite3",
        )

    async def init_db(self):
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)

    async def register(self, user_id) -> User:
        async with self.engine.begin() as conn:
            await conn.execute(insert(User).values(user_id=user_id))

    