import aiohttp
import asyncio
from bs4 import BeautifulSoup


async def searchID(id: int) -> dict:
    async with aiohttp.ClientSession() as session:
        async with session.get('https://ranobehub.org/api/ranobe/' + str(id)) as get:
            answer = await get.json()
            await session.close()
            return answer["data"]


async def getVolumes(id: int) -> dict:
    async with aiohttp.ClientSession() as session:
        async with session.get('https://ranobehub.org/api/ranobe/' + str(id) + '/contents') as get:
            answer = await get.json()
            await session.close()
            return answer["volumes"]


async def getLast() -> dict:
    async with aiohttp.ClientSession() as session:
        async with session.get('https://ranobehub.org/api/search') as get:
            answer = await get.json()
            return answer["resource"]


async def getPopular() -> dict:
    async with aiohttp.ClientSession() as session:
            async with session.get('https://ranobehub.org/api/search?take=1&sort=computed_rating') as get:
                answer = await get.json()
                return answer["resource"]


async def searchByName(query: str, count: int) -> dict:
    async with aiohttp.ClientSession() as session:
        async with session.get(f'https://ranobehub.org/api/fulltext/global?query={query}&take={count}') as get:
            answer = await get.json()
            return answer