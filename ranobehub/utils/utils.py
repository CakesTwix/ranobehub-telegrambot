from ranobehub.utils.api import getVolumes
from pykeyboard import InlineKeyboard, InlineButton


async def getChapterText(url: str) -> dict:
    async with aiohttp.ClientSession() as session:
        async with session.get('https://' + url) as gibText:
            text = await gibText.text()
        soup = BeautifulSoup(text, features="lxml")
        await session.close()
        array_text = soup.findAll('div', class_="ui text container")[1].findAll('p')
        return dict(name=soup.find('h1', class_="ui header").getText(),
                    text="\n".join(str(x) for x in array_text))

async def changeKeyboard(ranobe_id: int, selected_volume: int, selected_chapter: int) -> InlineKeyboard:
    keyboard = InlineKeyboard()
    volumes = await getVolumes(ranobe_id)

    keyboard.paginate(len(volumes), selected_volume, 'selectVolumes:' + ranobe_id + ':{number}')
    keyboard.paginate(len(volumes[selected_volume - 1]["chapters"]), selected_chapter, 'selectChapter:' + f"{ranobe_id}:{selected_volume}" + ':{number}')
    
    keyboard.row(
        InlineButton('Ссылка на главу', url=volumes[selected_volume - 1]["chapters"][selected_chapter - 1]["url"]),
    )

    # One volume - one file <3
    keyboard.row(
        InlineButton('epub', url=f"https://ranobehub.org/ranobe/{ranobe_id}/{selected_volume}/download.epub"),
        InlineButton('epub (img)', url=f"https://ranobehub.org/ranobe/{ranobe_id}/{selected_volume}/download.epub_img"),
    )
    keyboard.row(
        InlineButton('Назад', f'backButton?{ranobe_id}')
    )

    return keyboard


async def changeVolumeChapterInfo(ranobe_id: int, selected_volume: int, selected_chapter: int) -> str:
    volumes = await getVolumes(ranobe_id)
    volumes = volumes[selected_volume - 1]
    chapter = volumes["chapters"][selected_chapter - 1]

    text = f"**{volumes['name']}**\n**Статус:** {volumes['status']['name']}\n\n"
    text += f"**{chapter['name']}**"

    return text


def getRanobeCaption(data: dict) -> str:
    caption = f'**{data["names"]["rus"]}** \n\n'
    caption += f'**Год:** {data["year"]}\n'
    caption += f'**Статус:** {data["status"]["title"]}\n'
    caption += f'**Понравилось**: {data["rating"]} 👍\n'
    caption += f'**Теги: **'
    for tag in data["tags"]["genres"]:
        caption += f'#{tag["names"]["rus"].replace(" ","_")} '
    caption += f'\n```{data["synopsis"]}```'

    return caption


def getRanobeCaptionInline(data: dict) -> str:
    caption = f'**{data["names"]["rus"]}** \n\n'
    caption += f'**Статус:** {data["status"]}\n'
    caption += f'**Понравилось**: {data["rating"]} 👍\n'
    caption += f'\n```{data["synopsis"]}```'

    return caption