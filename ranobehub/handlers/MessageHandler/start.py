from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from sqlalchemy import insert, select
from sqlalchemy.exc import IntegrityError
from __main__ import db

@client_bot.on_message(filters.command(["start"]))
async def start(client, message):
    try:
        await db.register(message.from_user.id)
        logger.info(f"New user {message.chat.first_name} {message.chat.last_name}")
    except IntegrityError:
        logger.debug(f"Old user {message.chat.first_name} {message.chat.last_name}")
    
    await message.reply("Привет, я бот, который может парсить японские книжки. По всем проблемам и вопросам - @CakesTwix. Новости по боту - @CakesBot_news")
