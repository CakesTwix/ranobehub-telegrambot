from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from sqlalchemy import insert, select
from sqlalchemy.exc import IntegrityError
from ranobehub.utils.api import searchByName
from pyrogram.types import (InlineQueryResultArticle, InputTextMessageContent,
                            InlineKeyboardMarkup, InlineKeyboardButton)

@client_bot.on_message(filters.command(["search"]))
async def last(client, message):
    logger.debug(f"{message.chat.first_name} {message.chat.last_name}")
    
    if len(message.text.split())> 1:
        answer = await searchByName(message.text[8:], 12)
    else: 
        return
    
    titles = 'Результат поиска: \n\n'

    i = 1
    ranobe_id = []
    ranobe_dict = {}

    if answer == []:
        return await message.reply("**Ничего не нашел, попробуйте другой запрос**")
    elif answer[0]["meta"]["key"] != "ranobe":
        return await message.reply("**Ничего не нашел, попробуйте другой запрос**")

    for searchList in answer:
        if searchList["meta"]["key"] == "ranobe":
            for ranobe in searchList["data"]:
                titles += f'{i}. [{ranobe["names"]["rus"]}]({ranobe["url"]})\n'
                ranobe_id.append(ranobe['id'])
                ranobe_dict[ranobe['id']] = i
                i += 1

    # Generate keyboard
    reply_markup = [
        [
            InlineKeyboardButton(f'{ranobe_dict[el]}', f'getRanobe?{el}') for el in ranobe_id[x:x+4]
        ] for x in range(0, len(ranobe_id), 4)
    ]
    await message.reply(titles, parse_mode="markdown", reply_markup=InlineKeyboardMarkup(reply_markup))
    logger.info(f"{message.chat.first_name} {message.chat.last_name}")
