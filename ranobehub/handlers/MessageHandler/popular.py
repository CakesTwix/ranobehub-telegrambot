from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from sqlalchemy import insert, select
from sqlalchemy.exc import IntegrityError
from ranobehub.utils.api import getPopular
from pyrogram.types import (InlineQueryResultArticle, InputTextMessageContent,
                            InlineKeyboardMarkup, InlineKeyboardButton)

@client_bot.on_message(filters.command(["popular"]))
async def last(_, message):
    answer = await getPopular()
    titles = 'Популярное ранобэ: \n\n'

    i = 1
    ranobe_id = []
    ranobe_dict = {}
    for ranobe in answer:
        titles += f'{i}. [{ranobe["names"]["rus"]}]({ranobe["url"]}) | {ranobe["counts"]["volumes"]} | {ranobe["counts"]["chapters"]}\n'
        ranobe_id.append(ranobe['id'])
        ranobe_dict[ranobe['id']] = i
        i += 1

    # Generate keyboard
    reply_markup = [
        [
            InlineKeyboardButton(f'{ranobe_dict[el]}', f'getRanobe?{el}') for el in ranobe_id[x:x+4]
        ] for x in range(0, len(ranobe_id), 4)
    ]
    await message.reply(titles, parse_mode="markdown", reply_markup=InlineKeyboardMarkup(reply_markup))
    logger.info(f"{message.chat.first_name} {message.chat.last_name}")
