from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from ranobehub.utils.api import getLast
from pyrogram.types import (InlineQueryResultArticle, InputTextMessageContent,
                            InlineKeyboardMarkup, InlineKeyboardButton)


@client_bot.on_inline_query(filters.regex(r'^last'))
async def last_inline(_, inline_query):
    latestRanobes = await getLast()

    await inline_query.answer(
        results=[
            InlineQueryResultArticle(
                title="Помощь",
                input_message_content=InputTextMessageContent(
                    "Here's how to install **Pyrogram**"
                ),
                description="Список доступных команд",
                reply_markup=InlineKeyboardMarkup(
                    [
                        [InlineKeyboardButton(
                            "Ссылка на ранобэ",
                            url=ranobe["url"]
                        )]
                    ]
                )
            )
        ],
        cache_time=1
    )