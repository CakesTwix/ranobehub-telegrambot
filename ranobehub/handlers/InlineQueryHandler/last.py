from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from ranobehub.utils.api import getLast
from ranobehub.utils.utils import getRanobeCaptionInline
from pyrogram.types import (
    InlineQueryResultArticle,
    InputTextMessageContent,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
)


@client_bot.on_inline_query(filters.regex(r"^last"))
async def last_inline(_, inline_query):
    latestRanobes = await getLast()

    await inline_query.answer(
        results=[
            InlineQueryResultArticle(
                title=ranobe["names"]["rus"],
                input_message_content=InputTextMessageContent(
                    getRanobeCaptionInline(ranobe)
                ),
                url=ranobe["url"],
                description=f'{ranobe["counts"]["volumes"]} | {ranobe["counts"]["chapters"]}',
                reply_markup=InlineKeyboardMarkup(
                    [
                        [InlineKeyboardButton("Ссылка на ранобэ", url=ranobe["url"])],
                        [
                            InlineKeyboardButton(
                                "epub",
                                url=f"https://ranobehub.org/ranobe/{ranobe['id']}/download.epub",
                            ),
                            InlineKeyboardButton(
                                "epub (img)",
                                url=f"https://ranobehub.org/ranobe/{ranobe['id']}/download.epub_img",
                            ),
                        ],
                        [
                            InlineKeyboardButton(
                                "Выбрать главу", f"selectVolumes?{ranobe['id']}"
                            )
                        ],
                    ]
                ),
            )
            for ranobe in latestRanobes
        ],
        cache_time=1,
    )
