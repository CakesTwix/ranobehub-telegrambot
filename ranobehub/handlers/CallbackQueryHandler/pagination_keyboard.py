from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from pykeyboard import InlineKeyboard, InlineButton
from ranobehub.utils.api import getVolumes
from ranobehub.utils.utils import changeKeyboard, changeVolumeChapterInfo
from pyrogram.errors.exceptions.bad_request_400 import MessageNotModified

@client_bot.on_callback_query(filters.create(lambda _, __, query: "selectVolumes:" in query.data))
async def selectChapter(_, callback_query):
    logger.debug(f"{callback_query.data}")
    await callback_query.answer()
    
    ranobe_id = callback_query.data.split(':')[1]
    selected_page = int(callback_query.data.split(':')[2])
    volumes = await getVolumes(ranobe_id)

    try:
        await callback_query.edit_message_caption(await changeVolumeChapterInfo(ranobe_id, selected_page, 1), reply_markup=await changeKeyboard(ranobe_id, selected_page, 1))
    except MessageNotModified:
        await callback_query.answer()

@client_bot.on_callback_query(filters.create(lambda _, __, query: "selectChapter:" in query.data))
async def selectChapter(_, callback_query):
    logger.debug(f"{callback_query.data}")
    await callback_query.answer()
    
    ranobe_id = callback_query.data.split(':')[1]
    selected_volume = int(callback_query.data.split(':')[2])
    selected_chapter = int(callback_query.data.split(':')[3])

    try:
        await callback_query.edit_message_caption(await changeVolumeChapterInfo(ranobe_id, selected_volume, selected_chapter), reply_markup=await changeKeyboard(ranobe_id, selected_volume, selected_chapter))
    except MessageNotModified:
        await callback_query.answer()