from loguru import logger
import os
from pyrogram import filters
from ranobehub.app import client_bot
from ranobehub.utils.api import searchID
from ranobehub.utils.utils import getRanobeCaption
import aiohttp
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton


@client_bot.on_callback_query(filters.create(lambda _, __, query: "getRanobe?" in query.data or "backButton?" in query.data))
async def getRanobe(client, callback_query):
    logger.debug(f"{callback_query.data}")
    await callback_query.answer()
    
    data = await searchID(callback_query.data.split('?')[1])


    keyboard_ranobe = [
        [
            InlineKeyboardButton( 
                "Ссылка на ранобэ",
                url=data["url"]
            ),
            InlineKeyboardButton( 
                "Начать с первой главы",
                url=data["start_reading_url"]
            )
        ],
        [
            InlineKeyboardButton( 
                "epub",
                url=f"https://ranobehub.org/ranobe/{data['id']}/download.epub"
            ),
            InlineKeyboardButton( 
                "epub (img)",
                url=f"https://ranobehub.org/ranobe/{data['id']}/download.epub_img"
            )
        ],
        [
            InlineKeyboardButton(
                "Выбрать главу",
                f"selectVolumes?{data['id']}"
            )
        ]
    ]
    
    if "getRanobe?" in callback_query.data:
        async with aiohttp.ClientSession() as session:
            async with session.get(data['posters']['big']) as resp:
                photo_read = await resp.read()
            with open(f"{data['id']}.png", "wb") as f:
                f.write(photo_read)

        await callback_query.message.delete()
        await client.send_photo(callback_query.message.chat.id, f.name, getRanobeCaption(data), reply_markup=InlineKeyboardMarkup(keyboard_ranobe))
        os.remove(f.name)
    else:
        await callback_query.edit_message_caption(getRanobeCaption(data), reply_markup=InlineKeyboardMarkup(keyboard_ranobe))
    
    