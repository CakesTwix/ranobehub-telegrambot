from loguru import logger
from pyrogram import filters
from ranobehub.app import client_bot
from pykeyboard import InlineKeyboard, InlineButton
from ranobehub.utils.api import getVolumes
from ranobehub.utils.utils import changeKeyboard, changeVolumeChapterInfo


@client_bot.on_callback_query(filters.create(lambda _, __, query: "selectVolumes?" in query.data))
async def selectChapter(_, callback_query):
    logger.debug(f"{callback_query.data}")
    keyboard = InlineKeyboard()
    ranobe_id = callback_query.data.split('?')[1]
    volumes = await getVolumes(ranobe_id)

    await callback_query.edit_message_caption(await changeVolumeChapterInfo(ranobe_id, 1, 1), reply_markup=await changeKeyboard(ranobe_id, 1, 1))


    