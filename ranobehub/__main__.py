from . import app
from . import database
import asyncio
from loguru import logger
from ranobehub.database.functions import DB



async def main():
    async with database.engine.begin() as conn:
        await conn.run_sync(database.Base.metadata.create_all)

# Init logger
logger.add("out.log", backtrace=True, diagnose=True) 

# Create Database
logger.info("Init database")
db = DB()
app.client_bot.run(db.init_db())

# Run bot
logger.info("Bot is running")
app.client_bot.run()